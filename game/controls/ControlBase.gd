extends Node

export(Vector2) var action_direction = Vector2()
export(bool) var action_jump = false
export(bool) var action_umbrella = false
