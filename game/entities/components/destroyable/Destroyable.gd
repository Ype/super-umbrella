extends Node

export(int) var hit_points = 1

var entity

signal destroyed

func _entity_ready(entity):
	self.entity = entity

func take_damage(amount, dir):
	hit_points -= amount
	var platformer = entity.get_component(preload("res://entities/components/platformer/Platformer.gd"))
	if platformer != null:
		platformer.push(20*(dir + Vector2(0, -0.2)))
	if hit_points <= 0:
		self.destroy()

func destroy():
	emit_signal("destroyed")
