extends Control

# Node that manages stage changing and saving (so there's no need to use autoload)

# Stage_Resources
const stage_select_resource = preload("res://menus/stage_select/StageSelect.tscn")

#Variables
export(String) var stage_name = "test_stage"
onready var save_manager = preload("res://managers/saver/saver.gd").new()

#Only call load_stage on StageSelect screen!
func load_stage(stage_name,stage_path):
	# Remove the current level
	var StageSelect = self.get_node("StageSelect")
	self.remove_child(StageSelect)
	StageSelect.call_deferred("free")

	# Add the next level
	self.stage_name = stage_name
	var stage_resource = load(stage_path)
	var stage = stage_resource.instance()
	self.add_child(stage)

#Adds a flag to save_manager
func add_flag(flag):
	save_manager.prepare_flag(flag)

#Leaves a stage and return to StageSelect screen
func exit():
	# Remove the current level
	var stage = self.get_node("Stage")
	self.remove_child(stage)
	stage.call_deferred("free")

	# Add the next level
	self.stage_name = null
	var stage_select = stage_select_resource.instance()
	stage_select.connect("stage_selected",self,"load_stage")
	self.add_child(stage_select)

#Saves the game and return to StageSelect screen
func save_and_exit():
	assert(stage_name != null)
	#"test_stage" is a reserved name for test stages
	if stage_name != "test_stage":
		save_manager.save_stage(stage_name)
	exit()